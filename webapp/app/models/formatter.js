sap.ui.define([
	"sap/ui/w/clock/scr/moment"
], function (timer) {
	"use strict";

	return {

		blank: function(imp){
			imp = imp == undefined ?  "-" : imp;
			return imp;
		},

		unixTime: function(sTime){

			if(sTime === 0){
				return "-";
			} else {
				let time = moment(sTime).format("HH:mm");
				return time;
			}
		},

		welcome: function(msg){
			msg = msg == undefined ?  " " : msg;
			return "Welcome " + msg;	
		},

		unixDate: function(sDate){
			let date = moment(sDate).format("DD/MM")

			return date;
		},

		hours: function (sHourIn, sHourOut) {
			if((sHourIn === 0) && (sHourOut === 0)){
				return "-";
			}

			if(sHourOut === 0){
				sHourOut = new Date();
				sHourOut = sHourOut.getTime();
			}

			let hours = moment(sHourOut).diff(sHourIn, 'hours', true);
			hours = hours.toFixed(1);

			return hours;
		},

		percCalc: function (sPercIn, sPercOut) {
			if((sPercIn === 0) && (sPercOut === 0)){
				return 0;
			}

			if(sPercOut === 0){
				sPercOut = new Date();
				sPercOut = sPercOut.getTime();
			}

			let hours = moment(sPercOut).diff(sPercIn, 'hours', true);

			let perc = ((hours/8.5)*100);
			perc = Math.round(perc);

			return perc;
		}

		/*
		wikiLink: function(sSpace) {
			return "www.google.co.uk";
			var link = "https://en.wikipedia.org/wiki/";
			let singleArr = sSpace.split(" ");
			if(singleArr.length == 1){
				link = link.concat(sSpace);
				console.log(link);
				return link;
			} else {
				let single = "";
				for(let i = 0; i < singleArr.length; i++){
					if(i = 0){
						single = single.concat(singleArr[i])
					} else {
						single = single.concat("_" + singleArr[i])
					}
				}
				link = link.concat(sSpace);
				return link;
			}
		}
		*/
	};
});