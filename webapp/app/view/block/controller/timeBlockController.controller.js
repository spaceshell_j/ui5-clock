sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/w/clock/app/models/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Controller, JSONModel, formatter, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("sap.ui.w.clock.app.view.block.controller.timeBlockController", {

		formatter: formatter,

		onInit: function (oEvent) {
			//var rData = this.getOwnerComponent().getModel("remoteData");
			//oView.setModel(this.oModel);

		},


		peopleTimeSearch: function (oEvent) {

			// build filter array
			var aFilter = [];
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				aFilter.push(new Filter("name", FilterOperator.Contains, sQuery));
			}

			// filter binding
			var oList = this.getView().byId("timeTable");
			var oBinding = oList.getBinding("items");
			oBinding.filter(aFilter);
		}
	});
});